

## [0.5.0](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/compare/0.5.0..0.4.2) (2023-12-12)


### Features

* SAAS-26 Add .env Template ([b33cd8d](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/b33cd8dd26c56d91af043e7b396ad720175b3622))
* SAAS-27 Read Project URL from .env ([16f0909](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/16f0909cac81fe6ef89815e33d6c802d2db95795))

## [0.4.2](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/compare/0.4.2..0.4.1) (2023-11-25)


### Bug Fixes

* SAAS-24 Enable release publish ([3d7c117](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/3d7c1175d5a273a62b923a91ee7dd9709188ad4a))

## [0.4.1](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/compare/0.4.1..0.4.0) (2023-11-25)


### Bug Fixes

* SAAS-20 Check for .env file ([68f6158](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/68f615815039a62e8a32f4127456fa117d36d9e9))
* SAAS-23 Gen Gitlab Credentials for Release ([b053ecf](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/b053ecfd35d2836269eb1e238bede40a61a93a23))
* SAAS-24 Use Release job from default pipeline ([dc348cb](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/dc348cb3443fba6d13d3e5b6f5130aeb6434ef4b))
* SAAS-24 Use Release job from default pipeline ([40c4d4d](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/40c4d4d7386573309b880ca699438d5f7e86cf14))

## [0.4.0](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/compare/0.4.0..0.3.0) (2023-11-25)


### Features

* SAAS-19 Add script to generate .npmrc ([8658952](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/8658952e54ac7b4c4ffe45b4be638c467f916e8a))
* SAAS-21 Allow release to run locally ([3d078a4](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/3d078a45baf19bc5cd1763103fc328f8caaa685b))
* SAAS-22 Lint, test and build on pre-commit ([2e0e1d5](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/2e0e1d59d51a1f43fb942b62b10fc85717ca44ab))


### Bug Fixes

* SAAS-20 Move env-cmd to dependency ([913fbf8](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/913fbf81fa517fdd3b9397457557d1d48c2b2b50))

## [0.3.0](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/compare/0.3.0..0.2.0) (2023-11-25)


### Features

* SAAS-20 Execute post install only for dev ([a045ea4](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/a045ea4520a020e25eb43c97e6c79996d2fbcf33))

## 0.2.0 (2023-06-20)


### Features

* SAAS-6 fix husky and add commit lint ([e350ff3](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/e350ff353737fd248a81c515cca96b6f785e1115))
* SAAS-7 add release-it ([fd25c6d](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/fd25c6daddf017b59a4067bc296e664455069796))
* SAAS-8 add CI/CD ([962c5e7](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/962c5e730a0f3e22ab238200a74ba68f814dc62a))
* SAAS-9 add git reset command ([bbbd9bd](https://gitlab.com/singletonsd/00-proof-of-concepts/typescript/ts-common-library/commit/bbbd9bd4295f84fa4406adfec06bafb550dcb050))