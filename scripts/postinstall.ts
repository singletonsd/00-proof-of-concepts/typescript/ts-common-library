import { execSync } from 'child_process';
import { existsSync } from 'fs';

const RUN_POSTINSTALL = process.env.RUN_POSTINSTALL;
if (!RUN_POSTINSTALL) {
  console.log('Skip Post install as this is a consumer');
  process.exit(0);
}

if (!existsSync('.eslintrc.js')) execSync('yarn lint --write-file');

execSync('husky install');
