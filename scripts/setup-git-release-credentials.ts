import { execSync } from 'child_process';
import https from 'https';
import { getProjectId } from './common';

const PUBLIC_KEY_TITLE =
  process.env.GITLAB_DEPLOY_KEY_TITLE || 'TEST__GITLAB_RELEASE_CI_CD-';
const CI_CD_VAR_NAME =
  process.env.GITLAB_CI_CD_VAR_NAME || 'TEST__SSH_PRIVATE_KEY';

async function main() {
  try {
    if (!process.env.GITLAB_TOKEN) {
      console.error('GITLAB_TOKEN var is missing');
      process.exit(1);
    }
    // Get the origin remote URL
    const originRemoteUrl =
      process.env.GITLAB_PROJECT_SSH_URL ||
      execSync('git config --get remote.origin.url')
        .toString()
        .trim();

    // Extract the GitLab project name from the remote URL
    const projectFullName = originRemoteUrl.split(':')[1].replace('.git', '');
    // Retrieve project ID using GitLab API
    console.log(`Retrieving project ID for ${projectFullName}...`);
    const projectId = await getProjectId(projectFullName);

    // Display the project ID
    console.log(`Project ID: ${projectId}`);
    // Generate SSH key pair
    console.log('Generating SSH key pair...');
    execSync('ssh-keygen -t rsa -b 4096 -f deploy_key -N ""');

    // Extract public key
    const publicKey = execSync('cat deploy_key.pub')
      .toString()
      .trim();
    // Add public key as a deploy key in GitLab project
    console.log('Adding public key as a deploy key in the GitLab project...');
    await addDeployKey(projectId, publicKey);

    // Set private key as a GitLab CI/CD variable
    console.log('Setting private key as a GitLab CI/CD variable...');
    const privateKey = execSync('cat deploy_key')
      .toString()
      .trim();
    await setVariable(projectId, CI_CD_VAR_NAME, privateKey);
    await setVariable(projectId, 'GITLAB_TOKEN', process.env.GITLAB_TOKEN);

    // Clean up generated files
    console.log('Cleaning up generated files...');
    execSync('rm deploy_key deploy_key.pub');
  } catch (error) {
    console.error('An error occurred:', error);
    process.exit(1);
  }
}

function addDeployKey(projectId: number, publicKey: string): Promise<void> {
  return new Promise((resolve, reject) => {
    const options = {
      hostname: 'gitlab.com',
      path: `/api/v4/projects/${projectId}/deploy_keys`,
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
        'Content-Type': 'application/json',
      },
    };

    const req = https.request(options, res => {
      if (res.statusCode === 200) {
        let data = '';
        res.on('data', chunk => {
          data += chunk;
        });
        res.on('end', () => {
          const deployKeys = JSON.parse(data);
          const existingKey = deployKeys.find(
            (key: any) => key.title === PUBLIC_KEY_TITLE
          );
          if (existingKey) {
            console.log(
              `The deploy key with name ${PUBLIC_KEY_TITLE} already exists in the project.`
            );
            resolve();
          } else {
            createDeployKey();
          }
        });
      } else if (res.statusCode === 201) {
        createDeployKey();
      } else {
        reject(
          new Error(`Failed to add deploy key. Status code: ${res.statusCode}`)
        );
      }
    });

    req.on('error', error => {
      reject(error);
    });

    req.end();

    function createDeployKey() {
      const createOptions = {
        hostname: 'gitlab.com',
        path: `/api/v4/projects/${projectId}/deploy_keys`,
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
          'Content-Type': 'application/json',
        },
      };

      const createReq = https.request(createOptions, res => {
        if (res.statusCode === 201) {
          console.log('The deploy key has been added to the project.');
          resolve();
        } else {
          reject(
            new Error(
              `Failed to add deploy key. Status code: ${res.statusCode}`
            )
          );
        }
      });

      createReq.on('error', error => {
        reject(error);
      });

      createReq.write(
        JSON.stringify({
          title: PUBLIC_KEY_TITLE,
          key: publicKey,
          can_push: true,
        })
      );
      createReq.end();
    }
  });
}

function setVariable(
  projectId: number,
  key: string,
  value: string
): Promise<void> {
  return new Promise((resolve, reject) => {
    // Check if the variable already exists
    getVariable(projectId, key)
      .then(variable => {
        if (!!variable) {
          console.log(`The variable "${key}" already exists in the project.`);
          resolve();
        } else {
          createVariable();
        }
      })
      .catch(reject);

    function createVariable() {
      const options = {
        hostname: 'gitlab.com',
        path: `/api/v4/projects/${projectId}/variables`,
        method: 'POST',
        headers: {
          'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
          'Content-Type': 'application/json',
        },
      };

      const req = https.request(options, res => {
        if (res.statusCode === 201) {
          console.log(`The variable "${key}" has been added to the project.`);
          resolve();
        } else {
          reject(
            new Error(
              `Failed to set CI/CD variable. Status code: ${res.statusCode}`
            )
          );
        }
      });

      req.on('error', error => {
        reject(error);
      });

      req.write(JSON.stringify({ key, value, protected: true }));
      req.end();
    }
  });
}

function getVariable(projectId: number, varName: string): Promise<any> {
  return new Promise((resolve, reject) => {
    const options = {
      hostname: 'gitlab.com',
      path: `/api/v4/projects/${projectId}/variables/${varName}`,
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
      },
    };

    const req = https.get(options, res => {
      if (res.statusCode === 200) {
        let data = '';
        res.on('data', chunk => {
          data += chunk;
        });
        res.on('end', () => {
          const variables = JSON.parse(data);
          resolve(variables);
        });
      } else if (res.statusCode === 404) {
        resolve(undefined);
      } else {
        reject(
          new Error(
            `Failed to retrieve CI/CD variables. Status code: ${res.statusCode}`
          )
        );
      }
    });

    req.on('error', error => {
      reject(error);
    });

    req.end();
  });
}

main();
