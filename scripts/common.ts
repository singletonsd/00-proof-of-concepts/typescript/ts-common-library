import https from 'https';

export function getProjectId(projectName: string): Promise<number> {
  return new Promise((resolve, reject) => {
    const options = {
      hostname: 'gitlab.com',
      path: `/api/v4/projects?&search_namespaces=true&membership=true&simple=true&search=${encodeURIComponent(
        projectName
      )}`,
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_TOKEN,
      },
    };

    const req = https.get(options, res => {
      let data = '';
      res.on('data', chunk => {
        data += chunk;
      });
      res.on('end', () => {
        if (res.statusCode === 200) {
          const projects = JSON.parse(data);
          if (Array.isArray(projects) && projects.length > 0) {
            const projectId = projects[0].id;
            resolve(projectId);
          } else {
            reject(new Error(`Project not found: ${projectName}`));
          }
        } else {
          reject(
            new Error(
              `Failed to retrieve project ID. Status code: ${res.statusCode}`
            )
          );
        }
      });
    });

    req.on('error', error => {
      reject(error);
    });

    req.end();
  });
}
