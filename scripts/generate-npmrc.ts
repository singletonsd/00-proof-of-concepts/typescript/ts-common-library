import { writeFileSync, existsSync, readFileSync } from 'fs';
import { execSync } from 'child_process';
import { getProjectId } from './common';

const npmrcPath = '.npmrc';
const domain = 'gitlab.com'; // Replace with your domain name
const npmToken = process.env.GITLAB_TOKEN; // Make sure to set the NPM_TOKEN environment variable

// Function to read package.json file
function readPackageJson(): any {
  try {
    const packageJsonPath = './package.json'; // Assuming package.json is in the root directory
    const packageJsonData = readFileSync(packageJsonPath, 'utf8');
    return JSON.parse(packageJsonData);
  } catch (error) {
    console.error('Error reading package.json:', error);
    return null;
  }
}

// Function to get scope from package.json
function getScopeFromPackageJson(): string | null {
  const packageJson = readPackageJson();
  if (packageJson && packageJson.name) {
    const packageName: string = packageJson.name;
    const scopeMatch = packageName.match(/^@([^/]+)/);
    if (scopeMatch && scopeMatch[1]) {
      return scopeMatch[1];
    }
  }
  return null;
}

const scope = getScopeFromPackageJson();
if (scope) {
  console.log('Scope:', scope);
} else {
  console.error('No scope found in package.json');
  process.exit(1);
}

async function main() {
  if (existsSync(npmrcPath)) {
    console.log('.npmrc file already exists. Skipping creation.');
    process.exit(0);
  }
  if (!npmToken) {
    console.error('GITLAB_TOKEN var is missing');
    process.exit(1);
  }
  // Get the origin remote URL
  const originRemoteUrl = execSync('git config --get remote.origin.url')
    .toString()
    .trim();
  // Extract the GitLab project name from the remote URL
  const projectFullName = originRemoteUrl.split(':')[1].replace('.git', '');
  // Retrieve project ID using GitLab API
  console.log(`Retrieving project ID for ${projectFullName}...`);
  const projectID = await getProjectId(projectFullName);

  const npmrcContent = `@${scope}:registry=https://${domain}/api/v4/projects/${projectID}/packages/npm/
    //${domain}/api/v4/projects/${projectID}/packages/npm/:_authToken="${npmToken}"
    `;

  writeFileSync(npmrcPath, npmrcContent);
  console.log('.npmrc file created successfully!');
}

main();
